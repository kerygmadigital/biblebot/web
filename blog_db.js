const loki = require('lokijs');

const db = new loki('blog_posts');
const posts = db.getCollection('posts') || db.addCollection('posts', { indices: ['author', 'date']});

if (posts.find().length == 0) {
    posts.insert({
        title: 'Release: v9.2-beta',
        author: '186046294286925824',
        date: Date.parse('31 March 2022 00:00:00 EDT'),
        text: "We would like to express our gratitude to everyone who helped us in our fundraiser for the Biblica royalty fee. This release is dedicated to you."
    });

    posts.insert({
        title: 'Release: v9.2-beta',
        author: '186046294286925824',
        date: Date.parse('31 March 2022 02:00:00 EDT'),
        text: "We would like to express our gratitude to everyone who helped us in our fundraiser for the Biblica royalty fee. This release is dedicated to you."
    });

    posts.insert({
        title: 'Release: v9.2-beta',
        author: '186046294286925824',
        date: Date.parse('31 March 2022 04:00:00 EDT'),
        text: "We would like to express our gratitude to everyone who helped us in our fundraiser for the Biblica royalty fee. This release is dedicated to you."
    });
}

module.exports = posts;