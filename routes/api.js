const express = require('express');
const router = express.Router()

const auth = require('./auth');
const stats = require('./stats');
const posts = require('./blog');

router.use('/auth', auth);
router.use('/stats', stats);
router.use('/posts', posts);

module.exports = router;