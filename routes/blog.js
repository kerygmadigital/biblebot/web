const express = require('express');
const router = express.Router();

const db = require('../blog_db');
const biblebotDb = require('../biblebot_db');

router.get('/', async (req, res, next) => {
    const posts = db.find();

    for (let i = 0; i < posts.length; i++) {
        const user = await biblebotDb.User.findOne({UserId: posts[i].author});
        posts[i].author = user.UserName;
    }
    
    res.json(posts);
})

router.get('/last/:n', async (req, res, next) => {
    const today = Date.now();

    const posts = db.where(obj => {
        const date = obj.date;
        
        return date < today;
    });

    let nInt = 0;

    try {
        nInt = Number.parseInt(req.params.n);
    } catch {
        throw Error('cannot parse integer in url');
    }

    // Divide posts by nInt if amount of posts allows.
    const resp = posts.length >= nInt ? posts.slice(0, nInt) : posts;

    for (let i = 0; i < posts.length; i++) {
        /*
            These changes get cached... somehow, thus if we iterate over these posts after the author
            already been "corrected", getUserNameFromUserIdOrDefault() will return the default, as the
            author is no longer a simple user ID. This if statement prevents that from happening.
        */
        if (!posts[i].author.match(/[a-zA-Z]/))
            posts[i].author = await getUserNameFromUserIdOrDefault(posts[i].author);
    }

    res.json(resp);
});

router.get('/submit', (req, res, next) => {
    if (req.isAuthenticated()) {
        const user = req.user;
        const permittedUsers = ["186046294286925824", "304602975446499329", "270590533880119297"];

        if (permittedUsers.includes(user.UserId)) {
            db.insert(req.body);
        } else {
            throw Error('you are not a permitted user');
        }
    } else {
        throw Error('you are not authenticated');
    }
});

const getUserNameFromUserIdOrDefault = async (userId) => {
    const user = await biblebotDb.User.findOne({UserId: userId});
    return user ? user.UserName : 'Kerygma Digital';
}

module.exports = router;