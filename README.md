<p align="center">
    <a alt="BibleBot" href="https://biblebot.xyz">
        <img alt="BibleBot" width="400px" src="https://i.imgur.com/JVBY24z.png">
    </a>
    <br>
    <br>
    <a alt="Discord" href="https://biblebot.xyz/discord">
        <img alt="Discord" src="https://img.shields.io/discord/362503610006765568?label=discord&style=flat-square">
    </a>
    <a href="https://github.com/BibleBot/web/blob/master/LICENSE">
        <img alt="MPL-2.0" src="https://img.shields.io/github/license/BibleBot/web?style=flat-square">
    </a>
    <a href="#">
        <img alt="Total Lines of Code" src="https://img.shields.io/tokei/lines/github/BibleBot/web?style=flat-square">
    </a>
    <br>
</p>
<p align="center">
    Scripture from your Discord client to your heart.
</p>

## Internal Organization

This repository is a monolith. The `client` folder contains a React frontend with Storybook for component-driven development. The root of this repository contains files for backend/express, which hosts the built frontend.

## Special Thanks

To our financial supporters to help us keep this project's lights on.  
To our volunteer translators helping BibleBot be more accessible to everyone.  
To our licensing coordinators for helping us sift through all the darn permissions requests.  
To our support team for helping others use BibleBot.
